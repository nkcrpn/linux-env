" Remap for Colemak
noremap n j
noremap e k
noremap i l
noremap k n
noremap s i
noremap j e

" Remap l as end of 'line' key
nnoremap l $
nnoremap <C-l> J

" Destroy arrow keys in insert mode
inoremap <Up> <Nop>
inoremap <Down> <Nop>
inoremap <Left> <Nop>
inoremap <Right> <Nop>

" Make arrow keys useful
noremap <Up> ddkP
noremap <Down> ddp
noremap <Left> <<
noremap <Right> >>
vmap <Left> <gv
vmap <Right> >gv

" auto insert comments
setl fo+=r

" Make backspace work
set backspace=2

" Indent if already indented
set autoindent

" auto indent on these words
set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class

" Wrapping and tabs.
set tw=80 ts=4 sw=4 sta et sts=4 ai

" Highlight end of line whitespace.
highlight WhitespaceEOL ctermbg=240 guibg=red
match WhitespaceEOL /\s\+$/

" Auto completion via ctrl-space (instead of the nasty ctrl-x ctrl-o)
set omnifunc=pythoncomplete#Complete
inoremap <Nul> <C-x><C-o>

" More syntax highlighting.
let python_highlight_all = 1

" Line numbers
set number

" PLEASE STOP BEEPING OMG
set visualbell
set t_vb=

" Keep 5 line padding on cursor
set scrolloff=5

" keyword highlighting
syntax on

" Filetype recognition
filetype plugin on

" Color column at 161
set colorcolumn=81

" Pathogen
execute pathogen#infect()

" set color scheme to solarized
syntax on
let g:solarized_termcolors=256
set t_Co=256
set background=dark
colorscheme solarized

" Haskellmode configuration
au BufEnter *.hs compiler ghc
let g:haddock_browser = "/usr/bin/firefox"
let g:ghc = "/usr/local/bin/ghc"

au FileType html set ts=2 sts=2 sw=2
au FileType css set ts=2 sts=2 sw=2
au FileType scss set ts=2 sts=2 sw=2
au FileType php set ts=2 sts=2 sw=2
au FileType ruby set ts=2 sts=2 sw=2
au FileType eruby set ts=2 sts=2 sw=2
au FileType haml set ts=2 sts=2 sw=2
au FileType coffee set ts=2 sts=2 sw=2
au FileType javascript set ts=2 sts=2 sw=2

" to keep whitespace highlighting on recognized file types
au FileType * highlight WhitespaceEOL ctermbg=240 guibg=red
