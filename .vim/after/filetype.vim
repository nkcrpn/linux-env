if exists("did_load_filetypes")
    finish
endif

augroup filetypedetect
    au BufRead, BufNewFile *.coffee set ft=coffee
augroup END
